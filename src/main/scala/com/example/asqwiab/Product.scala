package com.example.asqwiab

trait Product {
  def name: String
  def price: Float
  def productId: Int
}
object Product

case class ProductImpl(productId: Int, name: String, price: Float) extends Product
object ProductImpl
