package com.example.asqwiab

trait OrderProduct extends Product {
  def orderId: Int
  def updated(name: String, price: Float): OrderProduct
}
object OrderProduct {

  val fetchQuery =
    """
      | SELECT *
      | FROM Product
      | WHERE ProductId IN $1
    """.stripMargin

  def fetchProducts(sql: String, args: Any*): Array[Product] = ???

  // main method
  def updateAll(ops: Array[OrderProduct]) = {
    val products = fetchProducts(fetchQuery, ops.map(_.productId)).map(p => (p.productId, p)).toMap
    (for (op <- ops) yield (op, products(op.productId))).map(t => t._1.updated(t._2.name, t._2.price))
  }
}

case class OrderProductImpl(orderId: Int, productId: Int, name: String, price: Float) extends OrderProduct {
  def updated(name: String, price: Float): OrderProduct = copy(name = name, price = price)
}
object OrderProductImpl
